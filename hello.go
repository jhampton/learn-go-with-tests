package main

import "fmt"

const englishHelloPrefix = "Hello, "
const spanish = "Spanish"
const spanishHelloPrefix = "Halo, "
const french = "French"
const frenchHelloPrefix = "Bonjour, "

func buildGreetingPrefix(language string) (prefix string) {
	switch language {
	case french:
		prefix = frenchHelloPrefix
	case spanish:
		prefix = spanishHelloPrefix
	default:
		prefix = englishHelloPrefix
	}
	return
}

// Hello returns the string hello, world
func Hello(name string, language string) string {
	if name == "" {
		name = "World"
	}

	return buildGreetingPrefix(language) + name
}

func main() {
	fmt.Println(Hello("world", ""))
}
