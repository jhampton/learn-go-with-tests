# learn-go-by-tests

Example of TDD development in Go

Each package is testable - to test all of them at once you can run a command similar to below

`$ go test ./ ./integers ./iteration -bench=. -v`

Include Benchmarks

`$ go test -bench=.`

Include verbose output

`$ go test -v`
