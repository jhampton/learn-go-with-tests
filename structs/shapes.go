package structs

import "math"

// Shape behavior
type Shape interface {
	Area() float64
}

// Triangle type
type Triangle struct {
	Base   float64
	Height float64
}

// Rectangle type
type Rectangle struct {
	Width  float64
	Height float64
}

// Circle type
type Circle struct {
	Radius float64
}

// Area calculates the area for a rectangle
func (r Rectangle) Area() float64 {
	return r.Width * r.Height
}

// Area calculates the area for a circle
func (c Circle) Area() float64 {
	return math.Pi * c.Radius * c.Radius
}

// Area calculates the area for a Triangle
func (t Triangle) Area() float64 {
	return (t.Base * t.Height) * 0.5
}

// Perimeter calculates the perimeter of a rectangle
func Perimeter(rectangle Rectangle) float64 {
	return 2 * (rectangle.Width + rectangle.Height)
}
