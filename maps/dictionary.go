package dictionary

// Dictionary errors
const (
	ErrNotFound         = Err("could not find the word you were looking for")
	ErrWordExists       = Err("word exists")
	ErrWordDoesNotExist = Err("word does not exist")
)

// Dictionary type
type Dictionary map[string]string

// Err type
type Err string

func (e Err) Error() string {
	return string(e)
}

// Delete removes a word from the dictionary
func (d Dictionary) Delete(word string) {
	delete(d, word)
}

// Update attempts to update the definition of an existing word
func (d Dictionary) Update(word, definition string) error {
	_, err := d.Search(word)
	switch err {
	case ErrNotFound:
		return ErrWordDoesNotExist
	case nil:
		d[word] = definition
	default:
		return err
	}
	return nil
}

// Search attempts to finds a word in the dictionary
func (d Dictionary) Search(word string) (string, error) {
	definition, ok := d[word]
	if !ok {
		return "", ErrNotFound
	}

	return definition, nil
}

// Add adds a word to the dictionary
func (d Dictionary) Add(word, definition string) error {
	_, err := d.Search(word)
	switch err {
	case ErrNotFound:
		d[word] = definition
	case nil:
		return ErrWordExists
	default:
		return err
	}

	return nil
}
